using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using DocumentFormat.OpenXml.Office2010.Excel;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Serilog;
using ShopBridge.Infrastructure.Utils.Configuration;
using ShopBridge.Infrastructure.Utils.Configuration.Context;
using ShopBridge.Infrastructure.Utils.Configuration.IContext;
using ShopBridge.Modules.Inventory.Logic.Registry;

namespace ShopBridge.Modules.Inventory.ApiService
{ /// <summary>
  /// his method gets called by the runtime. Use this method to add services to the container.
  /// </summary>  
    public class Startup
    {     

        public IConfiguration Configuration { get; }
        /// <summary>
        /// his method gets called by the runtime. Use this method to add services to the container.
        /// </summary>  
        public void ConfigureServices(IServiceCollection services)
        {
            string filename = Directory.GetCurrentDirectory()+"\\Log\\" + DateTime.UtcNow.ToString("ddMMyyyy") + "\\shopbridge.txt";
            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Verbose()
                .WriteTo.File(filename)
                .CreateLogger();
                               
             
            services.AddControllers();
            services.AddScoped<IDBContext, DBContext>();
            services.AddMvcCore().AddApiExplorer();
            services.AddScoped<ITenantProvider, TenantProvider>();
            services.AddScoped<IDBContext>(service =>
            {
                var provider = service.GetRequiredService<ITenantProvider>();
                return provider.GetTenant();
            });


            services.AddScoped(service =>
            {
                var provider = service.GetRequiredService<ITenantProvider>();
                return provider.GetTenant();
            });
            LogicRegistry.IncludeLogicRegistry(services);

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Microsoft.OpenApi.Models.OpenApiInfo { Title = "Shop Bridge", Version = "v1" });

                string xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                string xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
              
                c.CustomSchemaIds(x => x.FullName);

            });

            services.AddScoped<IDBContext>(service =>
            {
                var provider = service.GetRequiredService<ITenantProvider>();
                return provider.GetTenant();
            });
        }
        /// <summary>
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// </summary>
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseHttpsRedirection();


            app.UseRouting();
            app.UseCors();
            app.UseCors(builder => builder
             .AllowAnyHeader()
             .AllowAnyMethod()
             .SetIsOriginAllowed((host) => true)
             .AllowCredentials()
             );
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
            //This line enables the app to use Swagger, 
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Shop Bridge");


            });
        }
    }
}
